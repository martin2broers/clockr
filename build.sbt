name := "Clockr"
 
version := "0.1"
      
lazy val `clockr` = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.typesafeRepo("releases")

scalaVersion := "2.11.4"

libraryDependencies += ehcache
libraryDependencies += ws
libraryDependencies += guice
libraryDependencies += evolutions
libraryDependencies += "mysql" % "mysql-connector-java" % "latest.release"
libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.3.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.joda" % "joda-convert" % "1.7"
)
libraryDependencies += "joda-time" % "joda-time" % "2.9.9"
//unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

      