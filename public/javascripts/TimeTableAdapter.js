"use strict";

function TableAdapter(){

    this.newRow = function(){
        return $("table.table.times tr:last");
    };

    this.getRowById = function(id){
        var StringValue = id.toString();
        var toReturn = null;
        $("table.table.times tr td:first-child").each(function(k, v){
            if($(v).html() === StringValue){
                console.log("Found at row"+ (k + 1));
                toReturn = $(v).parent();
                console.log(toReturn);
            }
        });
        return toReturn;
    };

    this.getRowIndexById = function(id){
        var StringValue = id.toString();
        var toReturn = null;
        $("table.table.times tr td:first-child").each(function(k, v){
            if($(v).html() === StringValue){
                console.log("Found at row"+ (k + 1));
                toReturn = k + 1;
                console.log(toReturn);
            }
        });
        return toReturn;
    };


    /**
     *
     * @param {Time} time
     * @return {boolean} if table is successfully updated
     */
    this.addToTable = function(time){
        UserInstance.getUserById(time.triggeredBy, function(user){
            ClientsInstance.getClientById(time.client, function (client) {
                UserInstance.getUserById(client.id, function (clientUser) {
                    CompaniesInstance.getCompanyById(client.company, function (company) {
                        TableInstance.newRow().after("<tr>" +
                            "<td>" + time.id + "</td>" +
                            "<td>" + user.firstName + ' ' + user.lastName + "</td>" +
                            "<td>" + clientUser.firstName +" "+ clientUser.lastName + " @ " + company.name + "</td>" +
                            "<td>" + time.start + "</td>" +
                            "<td>" + time.stop + "</td>" +
                            "<td>None</td>" +
                            "<td>None</td>" +
                            "<td>" + time.note + "</td>" +
                            "<td><button type='button' class='btn btn-secondary' onclick='billed(" + time.id + ")'>Billed</button></td>" +
                            "</tr>");
                    });
                });
            });
        });

        return true;
    };

    this.updateRow = function(time){
        UserInstance.getUserById(time.triggeredBy, function(user){
            ClientsInstance.getClientById(time.client, function (client) {
                UserInstance.getUserById(client.id, function (clientUser) {
                    CompaniesInstance.getCompanyById(client.company, function (company) {
                        TableInstance.getRowById(time.id).html("<td>" + time.id + "</td>" +
                            "<td>" + user.firstName + ' ' + user.lastName + "</td>" +
                            "<td>" + clientUser.firstName +" "+ clientUser.lastName + " @ " + company.name + "</td>" +
                            "<td>" + time.start + "</td>" +
                            "<td>" + time.stop + "</td>" +
                            "<td>None</td>" +
                            "<td>None</td>" +
                            "<td>" + time.note + "</td>" +
                            "<td><button type='button' class='btn btn-secondary' onclick='billed(" + time.id + ")'>Billed</button></td>");
                    });
                });
            });
        });

        return true;
    };

    this.updateColumn = function(row, column, data){
        $("table.table.times tr:nth-child("+row+") td:nth-child("+column+")").html(data);
        return true;
    }
}

var TableInstance = new TableAdapter();