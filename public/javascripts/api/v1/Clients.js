function Clients(){

    /**
     * Gets a companyObject via callback
     * @param {int} clientId
     * @param {callback} callback
     */
    this.getClientById = function(clientId, callback){
        ApiHandlerInstance.get(Constants.getClientById(clientId), {}, callback);
    };

    this.getAllClientViews = function(callback){
        ApiHandlerInstance.get(Constants.allClientViews, {}, callback);
    };

    this.new = function(client, callback){
        ApiHandlerInstance.post(Constants.newClient, client, callback);
    };
}

var ClientsInstance = new Clients();