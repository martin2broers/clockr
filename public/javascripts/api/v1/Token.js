"use strict";

function Token(){
    var currentToken = null;

    this.get = function (callback){
        if(currentToken === null){
            //must retrieve a token
            $.ajax({
                url: Constants.sessionToken,
                method: "GET"
            }).done(function(data) {
                currentToken = data;
                if(typeof callback !== 'undefined')
                    callback(currentToken);
            });
        }
        if(typeof callback !== 'undefined')
            callback(currentToken);
    };

    this.invalidateToken = function(){
        currentToken = null;
    };
}

var tokenInstance = new Token();
tokenInstance.get(); // directly get the token