function Users(){
    var currentUser = null;

    this.getCurrentUser = function(callback){
        if(currentUser !== null)
            callback(currentUser);
        else
            ApiHandlerInstance.get(Constants.getMyAccount, null, callback);
    };

    /**
     * Gets a userObject via callback
     * @param {int} userId
     * @param {callback} callback
     */
    this.getUserById = function(userId, callback){
        ApiHandlerInstance.get(Constants.getUser(userId), null, callback);
    };

    this.newUser = function(user, callback){
        ApiHandlerInstance.post(Constants.newUser, user, callback);
    };

    this.deleteUser = function(id, callback, onfailure){
        ApiHandlerInstance.delete(Constants.getUser(id), {}, callback, onfailure);
    };


    this.all = function(callback){
        ApiHandlerInstance.get(Constants.allUsers, "", callback);
    };

}

var UserInstance = new Users();