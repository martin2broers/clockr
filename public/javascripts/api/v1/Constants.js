"use strict";

function C(){
    this.sessionToken = "/api/v1/get-token";
    this.newTime = "/api/v1/time/new";
    this.newUser = "/api/v1/user";
    this.stopTime = "/api/v1/time/stop";
    this.getMyAccount = "/api/v1/my-account";

    this.noteByID = function (id) {
        return "/api/v1/time/"+id+"/note";
    };


    this.getSetBilledURL = function (id, isBilled) {
        return "/api/v1/time/"+id+"/billed/"+isBilled;
    };

    this.getUser = function (id) {
        return "/api/v1/user/"+id;
    };

    this.allUsers = "/api/v1/users";

    this.getClientById = function (id) {
        return "/api/v1/client/"+id;
    };

    this.allClientViews = "/api/v1/clients-view";

    this.getCompanyById = function (id) {
        return "/api/v1/company/"+id;
    };


    this.newCompany = "/api/v1/company";
    this.newClient = "/api/v1/client";
    this.allCompanies = "/api/v1/companies";
}

var Constants = new C();