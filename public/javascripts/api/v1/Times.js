"use strict";

function Times(){

    var currentTimeVar; //simply import the already defined time if available

    this.getCurrentTime = function(){
         if(typeof currentTimeVar === "object" && currentTimeVar !== null){
            return currentTimeVar;
        }else if(typeof time === "object" && currentTimeVar !== null){
            currentTimeVar = time;
            return currentTimeVar
        }else{
            return null;
        }
    };


    this.newTime = function(clientID, callback){
        UserInstance.getCurrentUser(function(user){
            ApiHandlerInstance.post(Constants.newTime, new Time(0, user.id, clientID, null, null, "", false), [function(data){
                currentTimeVar = data;
            },callback]);
        });
    };

    this.stopTime = function(callback){
        if(TimesInstance.getCurrentTime() !== null){
            UserInstance.getCurrentUser(function(user){
                ApiHandlerInstance.post(Constants.stopTime, TimesInstance.getCurrentTime(), [function(data){
                    currentTimeVar = null;
                }, callback]);
            });
        }else{
            //
        }
    };

    this.noteByID = function(id, callback){
        UserInstance.getCurrentUser(function(user){
            ApiHandlerInstance.get(Constants.noteByID(id), {}, callback);
        });
    };

    this.setNoteByID = function(id, content, callback){
        UserInstance.getCurrentUser(function(user){
            ApiHandlerInstance.post(Constants.noteByID(id), content, callback);
        });
    };

    this.setBilledByID = function(id, isBilled, callback){
        UserInstance.getCurrentUser(function(user){
            ApiHandlerInstance.post(Constants.getSetBilledURL(id, isBilled), {}, callback);
        });
    }
}

var TimesInstance = new Times();