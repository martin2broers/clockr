function User(id, firstName, lastName, email, password){
    this.id = -1;
    this.firstName = null;
    this.lastName = null;
    this.email = null;
    this.password = null;

    this.fromArgs = function(id, firstName, lastName, email, password){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        return this;
    };
    this.fromJson = function(Obj){
        for (var prop in Obj) {
            if (Obj.hasOwnProperty(prop)) {
                this[prop] = Obj[prop];
            }
        }
    };
}