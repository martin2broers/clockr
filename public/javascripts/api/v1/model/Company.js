function Company(id, name, address, city, tel){
    this.id = id;
    this.name = name;
    this.address = address;
    this.city = city;
    this.tel = tel;
}

function Company(Obj){
    this.id = -1;
    this.name = null;
    this.address = null;
    this.city = null;
    this.tel = null;
    for (var prop in Obj) {
        if (Obj.hasOwnProperty(prop)) {
            if(prop === 'id'){
                this[prop] = parseInt(Obj[prop]);
            }else{
                this[prop] = Obj[prop];
            }
        }
    }
}