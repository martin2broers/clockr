function Time(id, triggeredBy, client, start, stop, note, billed){
    this.id = id;
    this.triggeredBy = triggeredBy;
    this.client = client;
    this.start = start;
    this.stop = stop;
    this.note = note;
    this.billed = billed;
}