function Client(){
    this.id = null;
    this.user = null;
    this.company = null;

    this.fromArgs = function (id, userId, companyId){
        this.id = id;
        this.user = userId;
        this.company = companyId;
        return this;
    };

    this.fromObject = function(Obj){
        for (var prop in Obj) {
            if (Obj.hasOwnProperty(prop)) {
                if(prop === 'id'){
                    this[prop] = parseInt(Obj[prop]);
                }else{
                    this[prop] = Obj[prop];
                }
            }
        }
        return this;
    }
}