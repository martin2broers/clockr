function Companies(){

    /**
     * Gets a companyObject via callback
     * @param {int} companyId
     * @param {callback} callback
     */
    this.getCompanyById = function(companyId, callback){
        ApiHandlerInstance.get(Constants.getCompanyById(companyId), null, callback);
    };

    this.getAll = function(callback){
        ApiHandlerInstance.get(Constants.allCompanies, {}, callback);
    };

    this.new = function(company, callback){
        ApiHandlerInstance.post(Constants.newCompany, company, callback);
    };
}

var CompaniesInstance = new Companies();