function ApiHandler() {

    this.get = function (url, data, callback) {
        tokenInstance.get(function (token) { //do this with a token
            $.ajax({
                url: url,
                method: "GET",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {
                    "Token": token
                },
                data: JSON.stringify(data),
                success: function (result, status, xhr) {
                    if (typeof callback !== 'undefined')
                        callback(result);
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log("get failed with: " + textStatus);
                    console.log("error: " + errorThrown);
                    if (textStatus === "403" || textStatus === 403) {
                        console.log("token is invalid, getting new one...");
                        tokenInstance.invalidateToken(); // set current token to null
                        ApiHandlerInstance.get(url, data, callback); //retry
                    }
                }
            });
        });
    };

    /**
     *
     * @param url
     * @param data
     * @param {String} callbacks
     */
    this.post = function (url, data, callbacks) {
        var jsonData = JSON.stringify(data);
        tokenInstance.get(function (token) { //do this with a token
            $.ajax({
                url: url,
                method: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {
                    "Token": token
                },
                data: jsonData,
                success: function (data) {
                    if (typeof callbacks !== 'undefined'){
                        if(Array.isArray(callbacks)){
                            for (var i = 0; i < callbacks.length; i++) {
                                callbacks[i](data);
                            }
                        }else{
                            callbacks(data);
                        }
                    }
                },
                error: function (xhr, status, error) {
                    console.log("get failed with: " + status);
                    console.log("error: " + error);
                    if (status === "403" || status === 403) {
                        console.log("token is invalid, getting new one...");
                        tokenInstance.invalidateToken(); // set current token to null
                        ApiHandlerInstance.get(url, data, callback); //retry
                    }
                }
            });
        });

    };

    /**
     *
     * @param url
     * @param data
     * @param {String} callbacks
     */
    this.delete = function (url, data, callbacks, failureCallbacks) {
        var jsonData = JSON.stringify(data);
        tokenInstance.get(function (token) { //do this with a token
            $.ajax({
                url: url,
                method: "DELETE",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {
                    "Token": token
                },
                data: jsonData,
                success: function (data) {
                    if (typeof callbacks !== 'undefined'){
                        if(Array.isArray(callbacks)){
                            for (var i = 0; i < callbacks.length; i++) {
                                callbacks[i](data);
                            }
                        }else{
                            callbacks(data);
                        }
                    }
                },
                error: function (xhr, status, error) {
                    console.log("get failed with: " + status);
                    console.log("error: " + error);
                    if (typeof failureCallbacks !== 'undefined'){
                        if(Array.isArray(failureCallbacks)){
                            for (var i = 0; i < failureCallbacks.length; i++) {
                                failureCallbacks[i](xhr.responseText);
                            }
                        }else{
                            failureCallbacks(xhr.responseText);
                        }
                    }
                    if (status === "403" || status === 403) {
                        console.log("token is invalid, getting new one...");
                        tokenInstance.invalidateToken(); // set current token to null
                        ApiHandlerInstance.get(url, data, callback); //retry
                    }
                }
            });
        });

    }
}

var ApiHandlerInstance = new ApiHandler();