function Dashboard() {
    this.popup = null;
    this.self = null;
    var clockButtonToggle = false;

    /**
     * Should be called when the document is ready
     */
    this.init = function(){
        self = this; //ref to self

        $("#selectClient").select2({
            placeholder: "Select a client",
            allowClear: true
        });

        if (typeof time !== 'undefined') {
            if (time.id !== null && time.id !== -1) {
                clockButtonToggle = true;
            }
        }

        $('#new-user').click(function () {
            self.createNewUser();
        });

        $('#new-company').click(function () {
            self.createNewCompany();
        });

        $('#new-client').click(function () {
            self.createNewClient();
        });

        $("#toggle-timer").click(function () {
            if (!clockButtonToggle) {
                var client = $("#selectClient").val();
                if (client !== "empty") {
                    //client selected now transforming in int
                    client = parseInt(client);
                    TimesInstance.newTime(client, function (data) {
                        $("#toggle-timer").removeClass("btn-success").html("Stop");
                        $("#toggle-timer").addClass("btn-danger");
                        $("#selectClient").prop("disabled", true);
                        clockButtonToggle = !clockButtonToggle;
                        TableInstance.addToTable(data);
                    });
                }
            } else {
                TimesInstance.stopTime(function (data) {
                    $("#toggle-timer").addClass("btn-success").html("Start");
                    $("#toggle-timer").removeClass("btn-danger");
                    $("#selectClient").prop("disabled", false);
                    TableInstance.updateRow(data);
                    clockButtonToggle = !clockButtonToggle;
                });
            }
        });
    };



    /**
     * Opens a popup to create a new {User}
     */
    this.createNewUser = function () {
        self.closePopup();
        self.popup = new Popup('Create new user',
            '<form id="new-user-form-target" class="form-horizontal" role="form">' +
            '<div class="popup-html-body">' +
            '<div class="form-group"><label class="control-label" for="form-firstName">First name</label><input class="form-control" id="form-firstName" placeholder="Enter first name" type="text" name="firstName"></div>' +
            '<div class="form-group"><label class="control-label" for="form-lastName">Last name</label><input class="form-control" id="form-lastName" placeholder="Enter last name" type="text" name="lastName"></div>' +
            '<div class="form-group"><label class="control-label" for="form-email">Email address</label><input class="form-control" id="form-email" placeholder="Enter email" type="text" name="email"></div>' +
            '<div class="form-group"><label class="control-label" for="form-pw">Password</label><input class="form-control" id="form-pw" placeholder="Enter password" type="password" name="password"></div>' +
            '</div>'
            , function () {
                var form = $('#new-user-form-target').serializeFormJSON();
                var user = new User(form);
                UserInstance.newUser(user, function (data) {
                    location.reload();
                });
            });

        self.popup.show();
    };

    /**
     * Opens a popup for confirmation to delete {User}
     */
    this.deleteUser = function (id) {
        self.closePopup();
        self.popup = new Popup('Delete user?',
            'Are you sure you want to delete this user?'
            , function (popup) {
                UserInstance.deleteUser(id, function(data){
                    location.reload();
                },function(data){
                    self.closePopup();
                    setTimeout(function(){
                        self.popup = new Popup('Error',
                            data
                            , function (popup) {

                            },null, false, 1);
                        self.popup.show();
                    }, 500);
                });
            }, null, false, 2);

        self.popup.show();
    };



    /**
     * Open a popup for the creation of a new {Client}
     */
    this.createNewClient = function () {
        CompaniesInstance.getAll(function (companies) {
            UserInstance.all(function (users) {

                var selectCompanies = "<div class=\"col-sm-6\"><select id='companiesSelect' class=\"form-control custom-select\">";
                for (i = 0; i < companies.length; i++) {
                    selectCompanies += '<option value="' + companies[i].id + '">' + companies[i].name + '</option>';

                }
                selectCompanies += "</select></div>";

                var selectUsers = "<div class=\"col-sm-6\"><select id='clientsSelect' class=\" form-controlcustom-select\">";
                for (i = 0; i < users.length; i++) {
                    selectUsers += '<option value="' + users[i].id + '">' + users[i].firstName + ' ' + users[i].lastName + '</option>';

                }
                selectUsers += "</select></div>";

                var userSelect = $("#clientsSelect");
                var companySelect = $("#companiesSelect");


                self.closePopup();


                self.popup = new Popup('Create new client',
                    '<form id="new-user-form-target" class="form-horizontal" role="form">' +
                    '<div class="popup-html-body">' +
                    '<div class="form-group">' +
                    '<label for="form-user" class="control-label">Select user</label>' +
                    selectUsers +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label for="form-user" class="control-label">Select company</label>' +
                    selectCompanies +
                    '</div>' +
                    '</div>'
                    , function (popup) {
                        //save the new client
                        var userId = parseInt($("#clientsSelect").val());
                        var companyId = parseInt($("#companiesSelect").val());
                        var client = new Client().fromArgs(-1, userId, companyId);
                        ClientsInstance.new(client, function (data) {
                            location.reload();
                        });
                    });
                self.popup.show();


                $("#companiesSelect").select2({
                    placeholder: "Select a company",
                    allowClear: true
                });
                $("#clientsSelect").select2({
                    placeholder: "Select a client",
                    allowClear: true
                });

            });
        });
    };

    /**
     * opens a popup to create a new {Company}
     */
    this.createNewCompany = function () {
        self.closePopup();
        self.popup = new Popup('Create new company',
            '<form id="new-company-form-target" class="form-horizontal" role="form">' +
            '<div class="popup-html-body">' +
            '<div class="form-group"><label class="control-label" for="form-name">Company name</label><input class="form-control" id="form-name" placeholder="Enter company name" type="text" name="name"></div>' +
            '<div class="form-group"><label class="control-label" for="form-address">Address</label><input class="form-control" id="form-address" placeholder="Enter address" type="text" name="address"></div>' +
            '<div class="form-group"><label class="control-label" for="form-city">City</label><input class="form-control" id="form-city" placeholder="Enter city" type="text" name="city"></div>' +
            '<div class="form-group"><label class="control-label" for="form-tel">Telephone</label><input class="form-control" id="form-tel" placeholder="Enter telephone" type="text" name="tel"></div>' +
            '</div>'
            , function () {
                var form = $('#new-company-form-target').serializeFormJSON();
                var company = new Company(form);
                CompaniesInstance.new(company, function (data) {
                    location.reload();
                });
            });

        self.popup.show();
    };


    /**
     * opens a popup to edit a {Company}.
     * @param {int} id - the id of the {Company} to edit.
     */
    this.editCompany = function (id) {
        CompaniesInstance.getCompanyById(id, function (data) {
            self.closePopup();

            self.popup = new Popup('Create new company',
                '<form id="edit-company-form-target">' +
                '<div class="popup-html-body">' +
                '<input type="hidden" name="id" value="' + id + '">' +
                '<div class="form-group"><label for="form-name">Company name</label><input id="form-name" placeholder="Enter company name" type="text" name="name" value="' + data.name + '"></div>' +
                '<div class="form-group"><label for="form-address">Address</label><input id="form-address" placeholder="Enter address" type="text" name="address" value="' + data.address + '"></div>' +
                '<div class="form-group"><label for="form-city">City</label><input id="form-city" placeholder="Enter city" type="text" name="city" value="' + data.city + '"></div>' +
                '<div class="form-group"><label for="form-tel">Telephone</label><input id="form-tel" placeholder="Enter telephone" type="text" name="tel" value="' + data.tel + '"></div>' +
                '</div>'
                , function () {
                    var form = $('#edit-company-form-target').serializeFormJSON();
                    var company = new Company(form);
                    CompaniesInstance.new(company, function (data) {
                        location.reload();
                    });
                });
            self.popup.show();
        });
    };


    /**
     * Opens a popup to edit a note of a {Time} object
     * @param {int} id - the {Time} object ID
     */
    this.createEditNote = function(id){
        TimesInstance.noteByID(id, function (note) {
            self.closePopup();
            self.popup = new Popup('Edit note',
                '<textarea>' + note + '</textarea>'
                , function (modal) {
                    TimesInstance.setNoteByID(id, tinyMCE.activeEditor.getContent(), function (data) {

                    });
                }, null, true);

            self.popup.show();

            tinymce.init({
                selector: 'textarea',
                height: 400,
                width: 700,
                theme: 'modern',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        });
    };


    /**
     * Sets the billed flag of {Time} object
     * @param id
     * @param billed
     */
    this.billed = function(id, billed){
        TimesInstance.setBilledByID(id, billed, function () {
            if (billed === 1)
                TableInstance.updateColumn(TableInstance.getRowIndexById(id), 9, "<button type=\"button\" class=\"btn btn-success\" onclick=\"billed(" + id + ", 0)\">Set unbilled</button>")
            else
                TableInstance.updateColumn(TableInstance.getRowIndexById(id), 9, "<button type=\"button\" class=\"btn btn-secondary\" onclick=\"billed(" + id + ", 1)\">Billed</button>")
        });
    };


    /**
     * Should be called before creating a new {Popup} so there are no bindings to objects inside the popup
     */
    this.closePopup = function(){
        if (self.popup !== null) {
            self.popup.unbind();
            delete self.popup;
        }
    };
}

var DashboardInstance = new Dashboard();

$(document).ready(function () {
    DashboardInstance.init();
});