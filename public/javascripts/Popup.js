/**
 * Opens a predefined modal based on params.
 *
 * @param {string} title
 * @param {string} data
 * @param onsave - Callback
 * @param onclose - Callback
 * @param {boolean} [large]
 * @param {int} [type]
 * @constructor
 */
function Popup(title, data, onsave, onclose, large, type){
    var popup = null;
    if((large === undefined || large === null) && (type  === undefined || type === 0)){
        popup = $('#myModal');
        popup.find(".modal-header").removeClass("modal-header-danger");
        popup.find(".modal-header").removeClass("modal-header-warning");
        popup.find('#accept').removeClass('btn-danger');
        popup.find('#accept').addClass('btn-success');
        popup.find('#accept').html('Save');
    }else if(large){
        popup = $('#bigModal');
        popup.find(".modal-header").removeClass("modal-header-danger");
        popup.find(".modal-header").removeClass("modal-header-warning");
        popup.find('#accept').removeClass('btn-danger');
        popup.find('#accept').addClass('btn-success');
        popup.find('#accept').html('Save');
    }else if(type === 1){
        //warning
        //modal-header-warning
        popup = $('#myModal');
        popup.find(".modal-header").removeClass("modal-header-danger");
        popup.find(".modal-header").addClass("modal-header-warning");
        popup.find('#accept').removeClass('btn-danger');
        popup.find('#accept').addClass('btn-warning');
        popup.find('#accept').html('Confirm');
    }else if(type === 2){
        //danger
        //modal-header-danger
        popup = $('#myModal');
        popup.find(".modal-header").removeClass("modal-header-warning");
        popup.find(".modal-header").addClass("modal-header-danger");
        popup.find('#accept').html('Confirm');
        popup.find('#accept').removeClass('btn-success');
        popup.find('#accept').addClass('btn-danger');
    }



    popup.find(".modal-header h4").html(title);
    popup.find(".modal-body").html(data);

    if(onsave !== 'undefined' && onsave !== null){
        popup.find('#accept').click(function(){
            onsave(this);
            popup.modal('hide');
        });
    }else{
        popup.find('#accept').click(function(){
            popup.modal('hide');
        });
    }

    popup.find('#close').click(function(){
        if(onclose !== undefined && onclose !== null){
            onclose(this);
        }
        popup.modal('hide');
    });


    this.show = function(){
        popup.modal();
    };

    this.setOnSuccess = function (f) {
        popup.find('#accept').click(function(){
            f(this);
            popup.modal('hide');
        });
    };

    this.unbind = function(){
        popup.find('#accept').unbind();
        popup.find('#close').unbind();
    };
}