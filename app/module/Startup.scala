package module

import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}
import startup.CreateSU

class Startup extends Module {
  def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = {
    Seq(
      bind[startup.Startup].to[CreateSU].eagerly()
    )
  }
}