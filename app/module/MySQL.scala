package module

import model.Slick._
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class MySQL extends Module {
  def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = {
    Seq(
      bind[UsersTrait].to[Users].eagerly(),
      bind[TimesTrait].to[Times].eagerly(),
      bind[ClientsTrait].to[Clients].eagerly()
    )
  }
}