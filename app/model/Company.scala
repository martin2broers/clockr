package model

case class Company(id: Int, name: String, address: String, city: String, tel: String)
