package model
import java.sql.Date

import org.joda.time.DateTime

case class Time(var id: Int = -1, triggeredBy: Int = -1, client: Int = -1, var start: Option[DateTime] = None, var stop: Option[DateTime] = None, note: String = "", billed: Boolean = false)
case class TimeView(id: Int, triggeredBy: User, client: Client, start: Option[DateTime], stop: Option[DateTime], note: String, billed: Boolean){
  def startToJoda:Option[DateTime] = {
    if(start.isDefined)
      Some(new DateTime(start.get))
    else
      None
  }

  def stopToJoda:Option[DateTime] = {
    if(stop.isDefined)
      Some(new DateTime(start.get))
    else
      None
  }
}
