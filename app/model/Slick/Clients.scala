package model.Slick

import javax.inject.{Inject, Singleton}

import model._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}

trait ClientsTrait

@Singleton
class Clients @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, users: Users, companies: Companies)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ClientsTrait{


  import profile.api._

  val tableRef = TableQuery[ClientTable]
  private val insertQuery = tableRef returning tableRef.map(_.id) into ((item, id) => item.copy(id = id))


  def getById(id: Int):Future[Option[Client]] = db.run(tableRef.filter(_.id === id).result.headOption)
  def getAll(limit: Int = 500):Future[Seq[Client]] = db.run(tableRef.take(limit).result)

  def save(client: Client):Future[Option[Client]] = db.run{
    tableRef.filter(_.id === client.id).map(result => (result.id, result.user_id, result.company_id))
      .update((client.id, client.user, client.company)).map {
      case 0 => None
      case _ => Some(client)
    }
  }

  def insert(client: Client):Future[Client] = db.run(insertQuery += client)


  private val getAllJoinedQuery = for {
    ((o, u), c) <- tableRef joinLeft users.tableRef on (_.user_id === _.id) joinLeft companies.tableRef on (_._1.company_id === _.id)
  } yield(o, u, c)

  private val compiledGetAllJoinedQuery = Compiled((d: ConstColumn[Long]) => getAllJoinedQuery.take(d).sortBy(_._1.id)) //will speed things up

  def getAllView(limit: Int = 500): Future[Seq[ClientView]] = db.run{
    compiledGetAllJoinedQuery(limit).result.map { (rows) =>
      val b = ArrayBuffer.empty[ClientView]
      for (row <- rows) {
        b += ClientView(id = row._1.id,
          userID = row._2.get.id,
          user = User(row._2.get.id, firstName = row._2.get.firstName, lastName = row._2.get.lastName, email = row._2.get.email, password = row._2.get.password),
          companyID = row._3.get.id,
          company = row._3.get
        )
      }
      b
    }
  }


  class ClientTable(tag: Tag) extends Table[Client](tag, "clients") {

    def id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    def user_id = column[Int]("user_id")
    def company_id = column[Int]("company_id")

    override def * = (id, user_id, company_id) <> ((Client.apply _).tupled, Client.unapply)
  }
}