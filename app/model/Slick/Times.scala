package model.Slick

import javax.inject.{Inject, Singleton}

import akka.pattern.FutureRef
import model._
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.sql.SqlProfile.ColumnOption.Nullable

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}

trait TimesTrait

@Singleton
class Times @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, val clients: Clients, val users: Users)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with TimesTrait{

  import profile.api._
  import com.github.tototoshi.slick.MySQLJodaSupport._

  val tableRef = TableQuery[TimesTable]

  private val insertQuery = tableRef returning tableRef.map(_.id) into ((item, id) => item.copy(id = id))

  private val getAllJoinedQuery = for {
    ((o, u), c) <- tableRef joinLeft users.tableRef on (_.triggeredBy_id === _.id) joinLeft clients.tableRef on (_._1.client_id === _.id)
  }yield(o, u, c)


  /**
    * Gets a [[Time]] object
    * @param id where id is the primary key of the time object
    */
  def getById(id: Int):Future[Option[Time]] = db.run(tableRef.filter(_.id === id).result.headOption)


  /**
    * Get the note(String) from the [[Time]] object by id
    * @param id where id is the primary key of the time object
    */
  def getNoteById(id: Int):Future[Option[String]] = db.run(tableRef.filter(_.id === id).map(_.note).result.headOption)


  /**
    * Updates the note text.
    * @param id where id is the primary key of the time object
    * @param content the content that overrides old
    * @return Future[Int] > 0 = updated
    */
  def setNoteById(id: Int, content: String):Future[Int] = db.run{
    val q = for { r <- tableRef if r.id === id } yield r.note
    q.update(content)
  }


  /**
    * Get [[Seq]] of [[Time]] by limit.
    * @param limit limit the amount taken, default is 500.
    */
  def getAll(limit: Int = 500):Future[Seq[Time]] = db.run(tableRef.take(limit).result)


  /**
    * Get all [[Time]] where stop is null
    * @param id where id is the primary key of the time object
    */
  def getActiveTime(id: Int):Future[Option[Time]] = db.run(tableRef.filter(r => r.triggeredBy_id === id && r.stop.isEmpty).result.headOption)


  /**
    * Sets the billed field to true or false.
    * @param switch 1 if billed must be set to true, false otherwise
    */
  def setBilled(id : Int, switch: Int):Future[Int] = db.run{
    val q = for { r <- tableRef if r.id === id } yield r.billed
    q.update(switch == 1)
  }


  /**
    * Inserts a [[Time]] object
    * @param time the [[Time]] object to be inserted
    * @return [[Time]] wrapped in [[Future]] with updated primary key
    */
  def insert(time: Time): Future[Time] = {
    time.start = Some(DateTime.now())
    val action = insertQuery += time
    db.run(action)
  }


  /**
    * Updates the stop time in the database.
    * @param time The [[Time]] object where the stop time needs to be set to now.
    * @return the [[Time]] with updated stop time.
    */
  def updateStopTime(time: Time):Future[Option[Time]] = db.run(tableRef.filter(_.id === time.id).map(r => r.stop).update(Some(DateTime.now())).map{
    case 0 => None
    case _ =>
      time.stop = Some(DateTime.now())
      Some(time)
  })


  /**
    * Can update all except the times.
    * @param time the [[Time]] object that needs to be saved/updated.
    * @return [[Time]] wrapped in [[Future]] with updated primary key.
    */
  def save(time: Time):Future[Option[Time]] =  db.run {
    tableRef.filter(_.id === time.id).map(result => (result.triggeredBy_id, result.client_id, result.billed, result.note)).update((time.triggeredBy, time.client, time.billed, time.note)).map {
      case 0 => None
      case _ => Some(time)
    }
  }

  /**
    * Get [[Seq]] with all [[TimeView]]
    * @param limit the amount to take, default is 500.
    */
  def getAllView(limit: Int = 500): Future[Seq[TimeView]] = db.run(getAllJoinedQuery.filter(_._1.billed === false).take(limit).result).map { (rows) =>
    val b = ArrayBuffer.empty[TimeView]
    for(row <- rows){
      b += TimeView(id = row._1.id,
        triggeredBy = User(row._2.get.id, firstName = row._2.get.firstName, lastName = row._2.get.lastName, email = row._2.get.email, password = row._2.get.password),
        client = Client(id = row._3.get.id, user = row._3.get.user, company = row._3.get.company),
        start = row._1.start,
        stop = row._1.stop,
        note = row._1.note,
        billed = row._1.billed
      )
    }
    b
  }

  class TimesTable(tag: Tag) extends Table[Time](tag, "times") {
    def id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    def triggeredBy_id = column[Int]("trigger_by_user_id")
    def client_id = column[Int]("client_id")
    def start = column[Option[DateTime]]("start", Nullable)
    def stop = column[Option[DateTime]]("stop", Nullable)
    def note = column[String]("note")
    def billed = column[Boolean]("billed")
    def triggeredBy = foreignKey("foreign_user_from_times", triggeredBy_id, users.tableRef)(_.id)
    def client = foreignKey("foreign_client_from_times", client_id, clients.tableRef)(_.id)

    def * = (id, triggeredBy_id, client_id, start, stop, note, billed) <> ((Time.apply _).tupled, Time.unapply)
  }
}