package model.Slick

import javax.inject.{Inject, Singleton}

import model.User
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


trait UsersTrait

@Singleton
class Users @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with UsersTrait{



  import profile.api._

  val tableRef = TableQuery[UsersTable]

  def getByEmail(email: String):Future[Option[User]] = db.run(tableRef.filter(_.email === email).result.headOption)
  def getById(id: Int):Future[Option[User]] = db.run(tableRef.filter(_.id === id).result.headOption)
  def deleteById(id: Int):Future[Int] = db.run{
    tableRef.filter(_.id === id).delete
  }

  private val insertQuery = tableRef returning tableRef.map(_.id) into ((item, id) => item.copy(id = id))
  def insert(user: User):Future[User] = db.run(insertQuery += user)

  def getAll(i: Int = 500):Future[Seq[User]] = db.run(tableRef.take(i).result)

  class UsersTable(tag: Tag) extends Table[User](tag, "users") {

    def id = column[Int]("ID", O.AutoInc, O.PrimaryKey)
    def firstName = column[String]("first_name")
    def lastName = column[String]("last_name")
    def email = column[String]("email")
    def password = column[Option[String]]("password")

    def * = (id, firstName, lastName, email, password) <> ((User.apply _).tupled, User.unapply)
  }
}