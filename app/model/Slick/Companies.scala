package model.Slick

import javax.inject.{Inject, Singleton}

import controllers.api.v1
import model.{Client, Company}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

trait CompaniesTrait

@Singleton
class Companies @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with CompaniesTrait{


  import profile.api._

  val tableRef = TableQuery[CompaniesTable]
  private val insertQuery = tableRef returning tableRef.map(_.id) into ((item, id) => item.copy(id = id))

  def getById(id: Int):Future[Option[Company]] = db.run(tableRef.filter(_.id === id).result.headOption)
  def getAll(limit: Int = 500):Future[Seq[Company]] = db.run(tableRef.take(limit).result)

  def insert(company: Company):Future[Company] = db.run(insertQuery += company)

  def save(company: Company):Future[Option[Company]] =  db.run {
    tableRef.filter(_.id === company.id).map(result => (result.name, result.address, result.city, result.tel))
      .update((company.name, company.address, company.city, company.tel)).map {
      case 0 => None
      case _ => Some(company)
    }
  }

  class CompaniesTable(tag: Tag) extends Table[Company](tag, "company") {

    def id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    def name = column[String]("name")
    def address = column[String]("address")
    def city = column[String]("city")
    def tel = column[String]("tel")

    def * = (id, name, address, city, tel) <> ((Company.apply _).tupled, Company.unapply)
  }
}

//case class Company(id: Long, name: String, address: String, city: String, tel: String)