package model.Json

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json._


trait APIv1 {

  implicit val ClientReads: Reads[model.Client] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "user").read[Int] and
      (JsPath \ "company").read[Int]
    ) (model.Client.apply _)

  implicit val ClientWrites: Writes[model.Client] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "user").write[Int] and
      (JsPath \ "company").write[Int]
    ) (unlift(model.Client.unapply))

  implicit val ClientViewReads: Reads[model.ClientView] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "userID").read[Int] and
      (JsPath \ "user").read[model.User] and
      (JsPath \ "companyID").read[Int] and
      (JsPath \ "company").read[model.Company]
    ) (model.ClientView.apply _)

  implicit val ClientViewWrites: Writes[model.ClientView] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "userID").write[Int] and
      (JsPath \ "user").write[model.User] and
      (JsPath \ "companyID").write[Int] and
      (JsPath \ "company").write[model.Company]
    ) (unlift(model.ClientView.unapply))

  implicit val CompanyReads: Reads[model.Company] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "name").read[String] and
      (JsPath \ "address").read[String] and
      (JsPath \ "city").read[String] and
      (JsPath \ "tel").read[String]
    ) (model.Company.apply _)

  implicit val CompanyWrites: Writes[model.Company] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "address").write[String] and
      (JsPath \ "city").write[String] and
      (JsPath \ "tel").write[String]
    ) (unlift(model.Company.unapply))


  val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val jodaDateReads = Reads[DateTime](js =>
    js.validate[String].map[DateTime](dtString =>
      DateTime.parse(dtString, DateTimeFormat.forPattern(dateFormat))
    )
  )

  implicit val jodaDateWrites: Writes[DateTime] = new Writes[DateTime] {
    def writes(d: DateTime): JsValue = JsString(d.toString())
  }

  implicit val timeReads: Reads[model.Time] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "triggeredBy").read[Int] and
      (JsPath \ "client").read[Int] and
      (JsPath \ "start").readNullable[DateTime] and
      (JsPath \ "stop").readNullable[DateTime] and
      (JsPath \ "note").read[String] and
      (JsPath \ "billed").read[Boolean]
    ) (model.Time.apply _)


  implicit val timeWrites: Writes[model.Time] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "triggeredBy").write[Int] and
      (JsPath \ "client").write[Int] and
      (JsPath \ "start").writeNullable[DateTime] and
      (JsPath \ "stop").writeNullable[DateTime] and
      (JsPath \ "note").write[String] and
      (JsPath \ "billed").write[Boolean]
    ) (unlift(model.Time.unapply))


  implicit val UserReads: Reads[model.User] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "firstName").read[String] and
      (JsPath \ "lastName").read[String] and
      (JsPath \ "email").read[String] and
      (JsPath \ "password").readNullable[String]
    ) (model.User.apply _)


  implicit val UserWrites: Writes[model.User] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "firstName").write[String] and
      (JsPath \ "lastName").write[String] and
      (JsPath \ "email").write[String] and
      (JsPath \ "password").writeNullable[String]
    ) (unlift(model.User.unapply))

  implicit val UserFormReads: Reads[model.UserForm] = (
    (JsPath \ "email").read[String] and
      (JsPath \ "password").read[String]
    ) (model.UserForm.apply _)


  implicit val UserFormWrites: Writes[model.UserForm] = (
    (JsPath \ "email").write[String] and
      (JsPath \ "password").write[String]
    ) (unlift(model.UserForm.unapply))
}
