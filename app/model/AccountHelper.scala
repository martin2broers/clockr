package model

import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

import org.apache.commons.codec.binary.Base64

import scala.util.Random

object AccountHelper {
  val salt = "7nQMHy1sUwpBklogApMPw1iiqItam2FNEbZNu3nefUaaU4qkDcBJFhJxc76QyDc89v22ZVlObiYi4FVKoXZuc2cqWmFjPxGBxiwQ"

  def hash(string: String): String = {
    var sha256: MessageDigest = null
    try
      sha256 = MessageDigest.getInstance("SHA-256")
    catch {
      case e: NoSuchAlgorithmException =>
        e.printStackTrace()
    }
    val passBytes = (string+salt).getBytes(StandardCharsets.US_ASCII)
    val passHash = Base64.encodeBase64(sha256.digest(passBytes))
    new String(passHash)
  }


  val random = "abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  def randomString(amount: Int): String = {
    val r = new Random()
    val b = new StringBuilder
    var i = 0
    while ( {
      i < amount
    }) {
      val index = r.nextInt(random.length)
      val c = random.charAt(index)
      b.append(c)

      {
        i += 1; i - 1
      }
    }
    b.toString
  }
}
