package model

import play.api.data.Form
import play.api.data.Forms._

case class User(id: Int, firstName: String, lastName: String, email: String, var password: Option[String])
case class UserForm(email: String, password: String)

object UserForm {

  val form = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText
    )(UserForm.apply)(UserForm.unapply)
  )
}

case class UserRegistrationForm(email: String, firstName: String, lastName: String)

object UserRegistrationForm {

  val form = Form(
    mapping(
      "email" -> email,
      "firstName" -> nonEmptyText,
      "lastName" -> nonEmptyText
    )(UserRegistrationForm.apply)(UserRegistrationForm.unapply)
  )
}