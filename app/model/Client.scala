package model

case class Client(id: Int, user: Int, company: Int)
case class ClientView(id: Int, userID: Int, user: User, companyID: Int, company: Company)
