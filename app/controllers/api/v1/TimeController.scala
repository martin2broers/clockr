package controllers.api.v1

import javax.inject.Inject

import akka.actor.ActorSystem
import controllers.SecuredController
import model.Slick.{Clients, Companies, Times, Users}
import model.Time
import play.api.libs.json.Json
import play.api.mvc.ControllerComponents

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class TimeController @Inject()(cc: ControllerComponents, urs: Users, times: Times, clients: Clients, companies: Companies, system: ActorSystem) extends SecuredController(cc, urs, system) with model.Json.APIv1 {

  def newClock = apiAuth(parse.json).async { request =>
    val time = request.body.validate[Time]
    if (time.isSuccess) {
      val action = times.insert(time.get)
      action.map(r => Ok(Json.toJson(r).toString()))
    } else {
      Future.successful(BadRequest)
    }
  }


  def stopClock = apiAuth(parse.json).async { request =>
    val timeJson = request.body.validate[Time]
    if (timeJson.isSuccess) {
      val time = times.getById(timeJson.get.id)
      time.map(r => {
        if (r.isDefined) {
          val saveAsync = times.updateStopTime(r.get)
          val save:Option[Time] = Await.result[Option[Time]](saveAsync, Duration.Inf)
          if (save.isDefined)
            Ok(Json.toJson(save.get).toString())
          else
            InternalServerError
        } else {
          InternalServerError
        }
      }
      )
    } else {
      Future.successful(BadRequest)
    }
  }


  /**
    * Get note by ID
    * @param id
    * @return
    */
  def getNoteById(id: Int) = apiAuth.async { request =>
    val action = times.getNoteById(id)
    action.map(r =>
      if(r.isDefined)
        Ok(Json.toJson(r.get).toString())
      else
        NotFound
    )
  }

  /**
    * Set note by ID
    * @param id
    * @return
    */
  def setNoteById(id: Int) = apiAuth(parse.json).async { request =>
    val content = request.Request.toString().replace("\"","")

    val action = times.setNoteById(id, content)
    action.map(r =>
      if(r > 0)
        Ok(Json.toJson(true).toString())
      else
        NotFound(Json.toJson(false).toString())
    )
  }



  /**
    * Set billed to 0 (not billed) or 1 (billed)
    * @param switch
    * @return
    */
  def setBilled(id: Int, switch: Int) = apiAuth.async { request =>
    val action = times.setBilled(id, switch)
    action.map(r =>
      if(r > 0)
        Ok(Json.toJson(true).toString())
      else
        NotFound
    )
  }


}
