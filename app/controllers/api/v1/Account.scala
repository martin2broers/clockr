package controllers.api.v1

import javax.inject.Inject

import akka.actor.ActorSystem
import controllers.SecuredController
import model.Slick.{Clients, Companies, Times, Users}
import model.{AccountHelper, Client, User, UserForm}
import play.api.libs.json.Json
import play.api.mvc.ControllerComponents

import scala.concurrent.Future

class Account @Inject()(cc: ControllerComponents, urs: Users, times: Times, clients: Clients, companies: Companies, system: ActorSystem) extends SecuredController(cc, urs, system) with model.Json.APIv1 {

  /**
    * Get a user by id
    * @param id
    * @return
    */
  def getUser(id: Int) = apiAuth.async { request =>
    val action = urs.getById(id)
    action.map(r =>
      if(r.isDefined)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }

  /**
    * Get a user by id
    * @param id
    * @return
    */
  def deleteUser(id: Int) = apiAuth.async { request =>
    val action = urs.deleteById(id)
    action.map(r =>
      if(r > 0)
        Ok(Json.toJson(true).toString())
      else
        NotFound(Json.toJson("This item could not be found").toString())
    ).recover { case _ => BadRequest(Json.toJson("Cannot delete, item is linked.").toString())}
  }


  /**
    * Gets all users as list and returns it
    * @return
    */
  def getAllUsers = apiAuth.async { request =>
    val action = urs.getAll()
    action.map(r =>
      if(r.nonEmpty)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }

  /**
    * inserts a new user
    * @return
    */
  def insertUser = apiAuth(parse.json).async { request =>
    val userInsert = request.body.validate[User]
    if(userInsert.isSuccess){
      val user = userInsert.get
      user.password = Some(AccountHelper.hash(user.password.get))
      urs.insert(userInsert.get).map(result =>
        Ok(Json.toJson(result).toString())
      )
    }else{
      Future.successful(BadRequest)
    }
  }


  /**
    * Get a user by id
    * @param id
    * @return
    */
  def getClientById(id: Int) = apiAuth.async { request =>
    val action = clients.getById(id)
    action.map(r =>
      if(r.isDefined)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }

  /**
    * Get a user by id
    * @return
    */
  def newOrSaveClient = apiAuth(parse.json).async { request =>
    val clientForm = request.body.validate[Client]
    if(clientForm.isSuccess){
      val client = clientForm.get
      if(client.id > 0){
        //update
        clients.save(client).map(result =>
          Ok(Json.toJson(result).toString())
        )
      }else{
        //insert
        clients.insert(client).map(result =>
          Ok(Json.toJson(result).toString())
        )
      }
    }else{
      Future.successful(BadRequest)
    }
  }


  /**
    * Get all the clients views
    * @return
    */
  def getAllClientsView = apiAuth.async { request =>
    val action = clients.getAllView()
    action.map(r =>
      if(r.nonEmpty)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }

  /**
    * Get all the clients
    * @return
    */
  def getAllClients = apiAuth.async { request =>
    val action = clients.getAll()
    action.map(r =>
      if(r.nonEmpty)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }

  /**
    * Get a user by id
    * @param id
    * @return
    */
  def getCompanyById(id: Int) = apiAuth.async { request =>
    val action = companies.getById(id)
    action.map(r =>
      if(r.isDefined)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }


  /**
    * Simply return info on the current account
    * Returns a token:String
    *
    * @return
    */
  def myAccount = auth { request =>
    Ok(Json.toJson(request.User))
  }

  /**
    * Simple generating a new token that can only be received if session is there
    * Returns a token:String
    *
    * @return
    */
  def requestNewToken = auth { request =>
    Ok(requestToken(request.User))
  }

  /**
    * Get a token based on login
    *
    * @return
    */
  def requestTokenWithLogin = Action(parse.json).async { request =>
    val userForm = request.body.validate[UserForm]
    if (userForm.isSuccess) {
      val userAsync = urs.getByEmail(userForm.get.email)
      userAsync.map(user =>
        if (user.isDefined) {
          Ok(requestToken(user.get))
        } else {
          BadRequest
        }
      )
    } else {
      Future.successful(BadRequest)
    }
  }
}
