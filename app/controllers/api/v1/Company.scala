package controllers.api.v1

import javax.inject.Inject

import akka.actor.ActorSystem
import controllers.SecuredController
import model.Slick.{Clients, Companies, Times, Users}
import model.{AccountHelper, User, UserForm}
import play.api.libs.json.Json
import play.api.mvc.ControllerComponents

import scala.concurrent.Future

class Company @Inject()(cc: ControllerComponents, urs: Users, companies: Companies, system: ActorSystem) extends SecuredController(cc, urs, system) with model.Json.APIv1 {


  /**
    * Get a user by id
    * @param id
    * @return
    */
  def getCompanyById(id: Int) = apiAuth.async { request =>
    val action = companies.getById(id)
    action.map(r =>
      if(r.isDefined)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }


  /**
    * Inserts a new [[Company]] to DB or if ID is defined the row will be updated
    * @return
    */
  def newOrEditCompany = apiAuth(parse.json).async { request =>
    val companyInsert = request.body.validate[model.Company]
    if(companyInsert.isSuccess){
      val company = companyInsert.get
      if(company.id > 0){
        //update
        companies.save(company).map(result =>
          Ok(Json.toJson(result).toString())
        )
      }else{
        //insert
        companies.insert(company).map(result =>
          Ok(Json.toJson(result).toString())
        )
      }
    }else{
      Future.successful(BadRequest)
    }
  }


  /**
    * Get all the clients
    * @return
    */
  def getAll = apiAuth.async { request =>
    val action = companies.getAll()
    action.map(r =>
      if(r.nonEmpty)
        Ok(Json.toJson(r).toString())
      else
        BadRequest
    )
  }

}
