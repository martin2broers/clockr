package controllers


import akka.actor.ActorSystem
import model.Slick.Users
import model.{AccountHelper, User}
import play.api.mvc._

import scala.collection.concurrent.TrieMap
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

object SecuredController{
  val userCache = TrieMap.empty[String, User]//TODO remove here and purely get from base model
  val TokenCache = TrieMap.empty[String, User] //TODO remove here and purely get from base model
}

class SecuredController(cc: ControllerComponents, users: Users, actorSystem: ActorSystem ) extends AbstractController(cc) {
  implicit val dashBoardContext: ExecutionContext = actorSystem.dispatchers.lookup("MySql")//use default for now



  lazy val apiAuth = ApiAuth()
  private def ApiAuth(customParser: BodyParser[Request[AnyContentAsJson]] = cc.parsers.defaultBodyParser.asInstanceOf[BodyParser[Request[AnyContentAsJson]]]) = new ActionBuilder[UserAuthenticatedRequest, Request[AnyContentAsJson]] {

    override def invokeBlock[A](request: Request[A], block: (UserAuthenticatedRequest[A]) => Future[Result]): Future[Result] = {
      val headers = request.headers.toSimpleMap
      if(headers.contains("Token") && SecuredController.TokenCache.contains(headers("Token"))){
        //great token is there you may pass
        block(new UserAuthenticatedRequest[A](SecuredController.TokenCache(headers("Token")), request))
      }else{
        Future.successful(Unauthorized)
      }
    }
    override def parser: BodyParser[Request[AnyContentAsJson]] = customParser
    override protected def executionContext: ExecutionContext = dashBoardContext
  }

  /**
    * Generates a token for a user
    * @param user
    * @return
    */
  protected def requestToken(user: User):String = {
    var token: String = AccountHelper.randomString(80)
    while(SecuredController.TokenCache.contains(token)){
      token = AccountHelper.randomString(80)
    }
    SecuredController.TokenCache.put(token, user)
    token
  }


  lazy val auth = Auth()
  private def Auth(customParser: BodyParser[Request[AnyContent]] = cc.parsers.defaultBodyParser.asInstanceOf[BodyParser[Request[AnyContent]]]) = new ActionBuilder[UserAuthenticatedRequest, Request[AnyContent]] {

    override def invokeBlock[A](request: Request[A], block: (UserAuthenticatedRequest[A]) => Future[Result]): Future[Result] = {
      val email = request.session.get("session.user_email")
      if(email.isDefined){
        if(SecuredController.userCache.contains(email.get)){
          val uar = new UserAuthenticatedRequest(SecuredController.userCache(email.get), request)
          block(uar)
        }else{
          val getUserFuture = users.getByEmail(email.get)
          Future{
            val result = Await.result(getUserFuture, Duration.Inf) //blocking action TODO improve
            if (result.isDefined) {
              SecuredController.userCache.put(email.get, result.get)
              val uar = new UserAuthenticatedRequest[A](result.get, request)
              val b = block(uar)
              Await.result(b, Duration.Inf) //another blocking action :/ TODO improve
            } else {
              Redirect("/login") withNewSession//for now
            }
          }
        }
      }else{
        Future.successful(Redirect("/login") withNewSession)
      }
    }
    override def parser: BodyParser[Request[AnyContent]] = customParser
    override protected def executionContext: ExecutionContext = dashBoardContext
  }
}

class UserAuthenticatedRequest[T](user: User, request: Request[T]) extends WrappedRequest(request){
  def User = user
  def Request = request.body
}