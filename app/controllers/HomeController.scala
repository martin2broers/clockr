package controllers

import javax.inject._

import akka.actor.ActorSystem
import model.Slick.{Clients, Companies, Times, Users}
import model.Time
import play.api.Logger
import play.api.mvc._
import play.twirl.api.Html
import views.html.component.{clock_time, start_stop_clock}

import scala.concurrent.Future

@Singleton
class HomeController @Inject()(cc: ControllerComponents, urs: Users, times: Times, clients: Clients, companies: Companies, system: ActorSystem) extends SecuredController(cc, urs, system) {


  def index = auth.async { request =>
    val startTime = System.currentTimeMillis
    val getAllTimesAsync = times.getAllView()
    val activeTimeAsync = times.getActiveTime(request.User.id)
    val getAllClientsAsync = clients.getAllView()
    for{
      getAllTimes <- getAllTimesAsync
      getAllClients <- getAllClientsAsync
      activeTime <- activeTimeAsync
    }yield Ok(views.html.dashboard("Clockr") {
      val endTime = System.currentTimeMillis
      Logger.info("retrieving alltimes and allclients, ms: "+ (endTime - startTime))
      Html(clock_time(getAllTimes, getAllClients.map(a => a.id -> a)(collection.breakOut)).body + start_stop_clock(getAllClients, activeTime.getOrElse(Time())).body)
    })
  }


  def users = auth.async{ request =>
    val getUsersAsync = urs.getAll()
    getUsersAsync.map(r => {
      Ok(views.html.dashboard("Clockr") {
        views.html.component.users(r)
      })
    })
  }

  def clientsPage = auth.async{ request =>
    val getAllClientsAsync = clients.getAllView()
    getAllClientsAsync.map(r => {
      Ok(views.html.dashboard("Clients") {
        views.html.component.clients(r)
      })
    })
  }


  def companiesPage = auth.async{ request =>
    val getAllCompanies = companies.getAll()
    getAllCompanies.map(r => {
      Ok(views.html.dashboard("Companies") {
        views.html.component.companies(r)
      })
    })
  }


}
