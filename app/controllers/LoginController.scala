package controllers

import javax.inject.Inject

import akka.actor.ActorSystem
import model.Slick.Users
import model.{AccountHelper, UserForm, UserRegistrationForm}
import play.api.mvc.ControllerComponents
import views.html.login

import scala.concurrent.Future

class LoginController @Inject() (cc: ControllerComponents, users : Users, system: ActorSystem) extends SecuredController(cc, users, system) with play.api.i18n.I18nSupport{
  implicit val executionContext = cc.executionContext

  def index = Action{ implicit request =>
    Ok(views.html.index("title"){
      login(UserForm.form)
    })
  }


  def onLogin = Action.async{implicit request =>
    UserForm.form.bindFromRequest.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        Future.successful(
          Redirect("/login") withNewSession
        )
      },
      userData => {
        val us = users.getByEmail(userData.email)
        us.map(user =>
          if(user.isDefined && user.get.password.getOrElse("none") == AccountHelper.hash(userData.password)){
            Redirect("/").withSession("session.user_email" -> userData.email)
          }else{
            Redirect("/login") withNewSession
          }
        )
      }
    )
  }


  def onNewUser() = auth.async{ implicit request =>
    UserRegistrationForm.form.bindFromRequest.fold(
      formWithErrors => {
        Future.successful(
          Redirect("/")
        )
      },
      userData => {

        Future.successful(Ok(""))
      }
    )
  }

  def logout = Action{
    Redirect("/") withNewSession
  }
}
