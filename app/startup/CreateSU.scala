package startup
import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import model.Slick.Users
import model.{AccountHelper, User}
import play.api.{Configuration, Logger}

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration


@Singleton
class CreateSU @Inject()(users: Users, config: Configuration, system: ActorSystem) extends Startup{

  implicit val ex: ExecutionContext = system.dispatchers.lookup("MySql")//use default for now

  users.getByEmail(config.get[String]("account.admin.name"))
  .map(result =>
    if(result.isEmpty){
      //need to create the superuser
      users.insert(
        User(1, "root", "root", config.get[String]("account.admin.name"), Some(AccountHelper.hash(config.get[String]("account.admin.pw"))))) // just insert the user
      .map(insertResult =>
          Logger.info("User created!")
      )
    }
  )
}
