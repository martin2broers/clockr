# clients schema

# --- !Ups


CREATE TABLE `clients` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `company_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `company_idx` (`company_id` ASC),
  INDEX `user_idx` (`user_id` ASC),
  CONSTRAINT `foreign_company`
    FOREIGN KEY (`company_id`)
    REFERENCES `company` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `foreign_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION);


# --- !Downs

DROP TABLE clients;