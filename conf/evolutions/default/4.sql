# clients clock

# --- !Ups

CREATE TABLE `times` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `trigger_by_user_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  `start` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop` DATETIME NULL DEFAULT NULL,
  `note` TEXT NULL,
  `billed` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `foreign_by_user_idx` (`trigger_by_user_id` ASC),
  INDEX `foreign_client_id_idx` (`client_id` ASC),
  CONSTRAINT `foreign_user_from_times`
    FOREIGN KEY (`trigger_by_user_id`)
    REFERENCES `users` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `foreign_client_from_times`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION);


# --- !Downs
DROP TABLE times;