# clients clock

# --- !Ups

ALTER TABLE `clients` ADD UNIQUE `unique_combination` (`user_id`, `company_id`);

# --- !Downs
DROP INDEX `unique_combination` ON `clients`